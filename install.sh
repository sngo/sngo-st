#!/bin/bash
PWD=$(pwd)
OLD_PWD=$PWD
PATCHES=$PWD/patches
FINAL_PATCHES=$PWD/final_patches
set -euo pipefail
# echo "==== Updating Submodule ==== "
# cd $PWD/st
# git reset origin/HEAD --hard
# make clean
# cd $OLD_PWD
# git submodule init
# git submodule update
cd $PWD/st
if [ -f "$OLD_PWD/config.h" ]; then
	set +e
	git branch -D config
	git checkout -b config
	retval=$?
	echo "CONFIG $retval"
	set -e
	echo "==== Copying config file ==== "
	cp $OLD_PWD/config.h $OLD_PWD/st/
	git add config.h
	git commit -am "Copying config"
	git checkout submo
	git merge config -m "Merging config"
fi
git checkout submo
echo "==== Creating Patch Branches ==== "
cd $OLD_PWD/st
for patch in $(ls $PATCHES); do
	echo "==== Creating branch for $patch ==== "
	set +e
	git branch -D $patch
	git checkout -b $patch
	retval=$?
	set -e
	if [[ ! $retval -eq 128 ]]; then
		git apply -3 --ignore-whitespace $PATCHES/$patch
		git commit -am "Applying $patch"
		git checkout submo
	fi
done
for patch in $(ls $PATCHES); do
	echo "==== Installing $patch ==== "
	set +e
	git merge $patch -m "Merging $patch"
	retval=$?
	set -e
	if [[ ! $retval -eq 0 ]]; then
		for file in $(git status | grep both | awk '{ print $3 }'); do
			sed -i '/^\(<<<<<<\|=======\|>>>>>>>\)\(.\)*$/d' $file
			git add $file
		done
		git commit -am "Merge fix $patch"
	fi
done
for patch in $(ls $FINAL_PATCHES); do
	git apply -3 --ignore-whitespace $FINAL_PATCHES/$patch
done
echo "==== Compiling and installing ==== "
cp $OLD_PWD/config.h $OLD_PWD/st/config.h
sudo make uninstall
sudo make clean install
cd $OLD_PWD
